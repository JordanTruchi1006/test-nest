import { Module } from '@nestjs/common';
import { HelloController } from './hello.controller';
import { HelloServiceModule } from '@Services/helloService/hello-service.module';

@Module({
  imports: [HelloServiceModule], // import dynamique de module
  controllers: [HelloController],
  // providers: [HelloService], // useful only if 1 module for each
})
export class HelloControllerModule {}
