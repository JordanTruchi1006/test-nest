import { Controller, Get, HttpCode, Query } from '@nestjs/common';
import { HelloService } from '@Services/helloService/hello.service';

@Controller('') // param is basinc controller path
export class HelloController {
  constructor(private readonly helloService: HelloService) {}

  @Get('hello') // param is route path
  @HttpCode(200) // default is 200
  getHello(@Query('firstName') firstName: string) {
    return this.helloService.getHello(firstName);
  }
}
