import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthServiceModule } from '@Services/authService/auth-service.module';
import { UserServiceModule } from 'src/services/userService/user-service.module';

@Module({
  imports: [AuthServiceModule, UserServiceModule], // import dynamique de module
  controllers: [AuthController],
  providers: [],
})
export class AuthControllerModule {}
