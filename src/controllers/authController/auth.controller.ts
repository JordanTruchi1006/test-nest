import {
  Controller,
  Get,
  Post,
  Request,
  Res,
  UnauthorizedException,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';
import { AuthService } from 'src/services/authService/auth.service';
import { UserService } from 'src/services/userService/user.service';
import { Public } from '../../services/authService/public.decorator';

@Controller('auth') // param is basic controller path
export class AuthController {
  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) {}

  @Public()
  @UseGuards(AuthGuard('local'))
  @Post('login')
  login(@Request() req, @Res() res): Response {
    // new properties aren't check
    // check if user's password exist with this email authService

    const tokens = this.authService.getTokens(req.user);

    res.cookie('refresh_token', tokens.refresh_token, {
      expires: new Date(new Date().getTime() + 2592000000),
      sameSite: 'strict',
      httpOnly: true,
    });
    return res.json({ access_token: tokens.access_token });
  }

  @Get('refresh')
  refresh(@Request() req, @Res() res): Response {
    // new properties aren't check
    // check if user's password exist with this email authService
    const refresh_token = req.cookies.refresh_token;

    const validToken = this.authService.verifyToken(refresh_token);

    if (validToken.status === false) throw new UnauthorizedException();
    const user = { email: validToken.email, userId: validToken.userId };
    const tokens = this.authService.getTokens(user);

    res.cookie('refresh_token', tokens.refresh_token, {
      expires: new Date(new Date().getTime() + 2592000000),
      sameSite: 'strict',
      httpOnly: true,
    });
    return res.json({ access_token: tokens.access_token });
  }
}
