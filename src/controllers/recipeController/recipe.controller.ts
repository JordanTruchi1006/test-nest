import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { CreateRecipeDTO } from '@Dto/recipe.dto';
import { Public } from '@Services/authService/public.decorator';
import { RecipeService } from '@Services/recipeService/recipe.service';
import { UserService } from '@Services/userService/user.service';
import { RecipeModel } from '@TsModels/recipe.model';

@Controller('recipes') // param is basic controller path
export class RecipeController {
  constructor(
    private readonly recipeService: RecipeService,
    private userService: UserService,
  ) {}

  @Public()
  @Get('') // param is route path
  async findAll(): Promise<Array<RecipeModel>> {
    return await this.recipeService.list();
  }

  @Public()
  @Get(':recipeId') // param is route path
  async getOne(@Param('recipeId', ParseIntPipe) recipeId: number) {
    return await this.recipeService.getOneById(recipeId);
  }

  @Public()
  @Post()
  async create(@Body() recipe: CreateRecipeDTO): Promise<RecipeModel> {
    // new properties aren't check
    console.log(recipe);
    const recipeInserted = await this.recipeService.createOne(recipe);
    return recipeInserted;
  }

  @Public()
  @Put(':recipeId')
  async updateOneFull(
    @Param('recipeId', ParseIntPipe) recipeId: number,
    @Body() recipe: CreateRecipeDTO,
  ): Promise<RecipeModel> {
    // new properties aren't check
    const recipeToUpdate = { ...recipe, id: recipeId };
    await this.recipeService.updateOneFull(recipeToUpdate);
    return recipeToUpdate;
  }

  /* @Delete(':recipeId')
  @HttpCode(204)
  deleteOne(@Param('recipeId', ParseIntPipe) recipeId: number): void { // new properties aren't check

    const recipes = this.recipeService.list();
    const indexItem = recipes.findIndex(elem => recipeId === elem.recipeId);
    recipes.splice(indexItem, 1);
  }

  @Patch(':recipeId')
  updateOnePartially(@Param('recipeId', ParseIntPipe) recipeId: number, @Body() recipe: CreateRecipeDTO): RecipeModel { // new properties aren't check

    const recipes = this.recipeService.list();
    const indexItem = recipes.findIndex(elem => recipeId === elem.recipeId);
    recipes[indexItem] = { ...recipes[indexItem], ...recipe};
    return recipe;
  } */
}
