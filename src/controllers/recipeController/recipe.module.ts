import { Module } from '@nestjs/common';
import { RecipeController } from '@Controllers/recipeController/recipe.controller';
import { RecipeServiceModule } from '@Services/recipeService/recipe-service.module';
import { UserServiceModule } from '@Services/userService/user-service.module';

@Module({
  imports: [RecipeServiceModule, UserServiceModule], // import dynamique de module
  controllers: [RecipeController],
})
export class RecipeControllerModule {}
