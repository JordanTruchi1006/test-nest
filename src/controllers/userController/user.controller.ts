import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from '@Services/userService/user.service';
import { CreatUserDTO } from '@Dto/user.dto';
import { UserModel } from '@TsModels/user.model';
import * as bcrypt from 'bcrypt';
import { saltOrRounds } from '@Controllers/userController/saltConstant';
import { Public } from '@Services/authService/public.decorator';

@Controller('user') // param is basic controller path
export class UserController {
  constructor(private userService: UserService) {}

  @Public()
  @Post()
  async create(@Body() user: CreatUserDTO): Promise<UserModel> {
    // new properties aren't check

    user.password = await bcrypt.hash(user.password, saltOrRounds);
    const userInserted = await this.userService.createOne(user);
    return userInserted;
  }
}
