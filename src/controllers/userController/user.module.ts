import { Module } from '@nestjs/common';
import { UserController } from '@Controllers/userController/user.controller';
import { UserServiceModule } from '@Services/userService/user-service.module';

@Module({
  imports: [UserServiceModule], // import dynamique de module
  controllers: [UserController],
})
export class UserControllerModule {}
