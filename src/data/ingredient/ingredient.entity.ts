import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { RecipeIngredientEntity } from '../recipes-ingredients/recipe-ingredient.entity';
@Entity()
export class IngredientEntity {
  @PrimaryGeneratedColumn({ name: 'ingredientId' })
  id: number;

  @Column()
  display_index: number;

  @Column()
  name: string;

  @Column()
  department: string;

  @Column()
  unit: string;

  @OneToMany(
    () => RecipeIngredientEntity,
    (recipeToIngredient) => recipeToIngredient.ingredient,
    {
      cascade: ['insert', 'update'],
      onDelete: 'CASCADE',
      orphanedRowAction: 'delete',
    },
  )
  recipeToIngredients: RecipeIngredientEntity[];
}
