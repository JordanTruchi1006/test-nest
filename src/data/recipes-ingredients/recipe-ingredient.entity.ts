import { IngredientEntity } from '@Data/ingredient/ingredient.entity';
import { RecipeEntity } from '@Data/recipe/recipe.entity';
import { Column, Entity, ManyToOne } from 'typeorm';

@Entity()
export class RecipeIngredientEntity {
  @Column()
  quantity!: number;

  @ManyToOne(
    () => IngredientEntity,
    (ingredient) => ingredient.recipeToIngredients,
    {
      primary: true,
      onDelete: 'CASCADE',
    },
  )
  ingredient!: IngredientEntity;

  @ManyToOne(() => RecipeEntity, (recipe) => recipe.recipeToIngredients, {
    primary: true,
    cascade: ['insert', 'update'],
    onDelete: 'CASCADE',
    orphanedRowAction: 'delete',
  })
  recipe!: RecipeEntity;
}
