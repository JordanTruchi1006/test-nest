import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { RecipeIngredientEntity } from '../recipes-ingredients/recipe-ingredient.entity';

@Entity()
export class RecipeEntity {
  @PrimaryGeneratedColumn({ name: 'recipeId' })
  id: number;

  @Column()
  title: string;

  @Column()
  image_name: string;

  @Column()
  instructions: string;

  @Column()
  servings: number;

  @OneToMany(
    () => RecipeIngredientEntity,
    (recipeToIngredient) => recipeToIngredient.recipe,
    {
      cascade: ['insert', 'update'],
      onDelete: 'CASCADE',
      orphanedRowAction: 'delete',
    },
  )
  recipeToIngredients: RecipeIngredientEntity[];
}
