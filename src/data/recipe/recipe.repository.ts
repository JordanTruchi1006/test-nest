import { EntityRepository, Repository } from 'typeorm';
import { RecipeEntity } from '@Data/recipe/recipe.entity';

@EntityRepository(RecipeEntity)
export class RecipeRepository extends Repository<RecipeEntity> {
  async findOneWithRelations(id: number, relations: Array<string>) {
    return await this.findOne(id, { relations: [...relations] });
  }

  async findOneWithRelationsBuilder(recipeId: number) {
    return await this.createQueryBuilder('recipe_entity')
      .leftJoinAndSelect('recipe_entity.ingredients', 'ingredients')
      .where('recipe_entity.recipeId = :recipeId', { recipeId })
      .getOne();
  }
}
