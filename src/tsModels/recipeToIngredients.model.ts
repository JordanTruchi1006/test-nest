export interface recipeToIngredientsModel {
  recipeId?: number;
  ingredientId?: number;
  quantity: number;
}
