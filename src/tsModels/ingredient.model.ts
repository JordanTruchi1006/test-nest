export interface IngredientModel {
  display_index: number;
  name: string;
  department: string;
  quantity: number;
  unit: string;
}
