import { recipeToIngredientsModel } from '@TsModels/recipeToIngredients.model';

export interface RecipeModel {
  recipeId?: number;
  title: string;
  image_name: string;
  instructions: string;
  servings: number;
  recipeToIngredients: Array<recipeToIngredientsModel>;
}
