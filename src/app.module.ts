import { AuthControllerModule } from '@Controllers/authController/auth.module';
import { HelloControllerModule } from '@Controllers/helloController/hello.module';
import { RecipeControllerModule } from '@Controllers/recipeController/recipe.module';
import { UserControllerModule } from '@Controllers/userController/user.module';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as typeormConfig from '@Root/ormconfig.json';
import { JwtAuthGuard } from '@Services/authService/jwt_auth_guard';
import { AppLoggerMiddleware } from '@Middlewares/logger';

@Module({
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
  imports: [
    HelloControllerModule,
    RecipeControllerModule,
    UserControllerModule,
    AuthControllerModule,
    TypeOrmModule.forRoot(<TypeOrmModuleOptions>typeormConfig),
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(AppLoggerMiddleware).forRoutes('*');
  }
}
