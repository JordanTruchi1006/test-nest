import { Injectable } from '@nestjs/common';

@Injectable()
export class HelloService {
  getHello(name: string) {
    return name ? { world: `Salut ${name} !` } : { world: 'Salut !' };
  }
}
