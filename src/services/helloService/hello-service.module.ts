import { Module } from '@nestjs/common';
import { HelloService } from './hello.service';

@Module({
  imports: [],
  controllers: [],
  providers: [HelloService],
  exports: [HelloService],
})
export class HelloServiceModule {}
