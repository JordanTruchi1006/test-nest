import { Module } from '@nestjs/common';
import { RecipeService } from '@Services/recipeService/recipe.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RecipeRepository } from '@Data/recipe/recipe.repository';
import { IngredientEntity } from '@Data/ingredient/ingredient.entity';

@Module({
  imports: [TypeOrmModule.forFeature([RecipeRepository, IngredientEntity])],
  controllers: [],
  providers: [RecipeService],
  exports: [RecipeService],
})
export class RecipeServiceModule {}
