import { Injectable } from '@nestjs/common';

import { RecipeModel } from '@TsModels/recipe.model';

import { RecipeRepository } from '@Data/recipe/recipe.repository';

@Injectable()
export class RecipeService {
  constructor(private recipeRepository: RecipeRepository) {}

  async list(): Promise<Array<RecipeModel>> {
    return await this.recipeRepository.find();
  }

  async getOneById(id: number): Promise<RecipeModel> {
    const relationsWanted = [
      'recipeToIngredients',
      'recipeToIngredients.ingredient',
    ];
    return await this.recipeRepository.findOneWithRelations(
      id,
      relationsWanted,
    );
  }

  async createOne(recipe: RecipeModel): Promise<RecipeModel> {
    await this.recipeRepository.save(recipe);
    return recipe;
  }

  async updateOneFull(recipe: RecipeModel): Promise<RecipeModel> {
    console.log(recipe);
    await this.recipeRepository.save(recipe);
    return recipe;
  }
}
