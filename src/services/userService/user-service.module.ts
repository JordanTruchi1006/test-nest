import { Module } from '@nestjs/common';
import { UserService } from '@Services/userService/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '@Data/user/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  controllers: [],
  providers: [UserService],
  exports: [UserService],
})
export class UserServiceModule {}
