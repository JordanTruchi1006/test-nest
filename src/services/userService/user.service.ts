import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '@Data/user/user.entity';
import { UserModel } from '@TsModels/user.model';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
  ) {}

  async createOne(user: UserModel): Promise<UserModel> {
    return await this.userRepository.save(user);
  }
}
