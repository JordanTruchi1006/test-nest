import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from '@Data/user/user.entity';
import { UserModel } from '@TsModels/user.model';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from '@Services/authService/jwtConstants';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<UserModel> {
    let userValid = await this.userRepository
      .createQueryBuilder('user')
      .addSelect('user.password')
      .where('user.email = :email', { email })
      .getOne();

    const isMatch = await bcrypt.compare(password, userValid.password);

    if (isMatch) delete userValid.password;
    else userValid = null;

    return userValid;
  }

  getTokens(user: UserModel): any {
    return {
      access_token: this.jwtService.sign(
        { ...user },
        { secret: jwtConstants.accessSecret, expiresIn: '30m' },
      ),
      refresh_token: this.jwtService.sign(
        { ...user },
        { secret: jwtConstants.refreshSecret, expiresIn: '30d' },
      ),
    };
  }

  verifyToken(refreshToken: string): any {
    const validToken = this.jwtService.verify(refreshToken, {
      secret: jwtConstants.refreshSecret,
    });
    if (validToken?.userId) return validToken;
    else return { status: false };
  }
}
