import { recipeToIngredientsModel } from '@TsModels/recipeToIngredients.model';

import {
  IsNotEmpty,
  IsNumber,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateRecipeDTO {
  @IsNotEmpty()
  @MinLength(5)
  @IsString()
  @MaxLength(100)
  title: string;

  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(50)
  @IsString()
  image_name: string;

  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(5000)
  @IsString()
  instructions: string;

  @IsNotEmpty()
  @IsNumber()
  servings: number;

  @IsNotEmpty()
  recipeToIngredients: Array<recipeToIngredientsModel>;
}
