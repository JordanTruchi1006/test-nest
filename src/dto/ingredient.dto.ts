import {
  IsNotEmpty,
  IsNumber,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateIngredientDTO {
  @IsNotEmpty()
  @IsNumber()
  display_index: number;

  @IsNotEmpty()
  @MinLength(5)
  @IsString()
  @MaxLength(100)
  name: string;

  @IsNotEmpty()
  @MinLength(5)
  @IsString()
  @MaxLength(100)
  department: string;

  @IsNotEmpty()
  @IsString()
  unit: string;

  @IsNotEmpty()
  @IsNumber()
  quantity: number;
}
