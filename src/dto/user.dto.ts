import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsString,
  IsEmail,
} from 'class-validator';

export class CreatUserDTO {
  @IsNotEmpty()
  @IsEmail()
  @IsString()
  @MaxLength(150)
  email: string;

  @IsNotEmpty()
  @MinLength(8)
  @IsString()
  @MaxLength(100)
  password: string;
}
