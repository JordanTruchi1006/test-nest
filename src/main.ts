import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import * as cookieParser from 'cookie-parser';
import { AppModule } from '@Src/app.module';
import { join } from 'path';
import * as csurf from 'csurf';
import * as express from 'express';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.use(cookieParser());
  app.enableCors({
    origin: 'localhost',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204,
  });
  /* app.use(csurf({ cookie: true })); */
  app.use(helmet());

  app.setGlobalPrefix('api'); // set base path for API, allow to avoid to repeat base path on each controllers or routes

  // work
  app.use('/public/img', express.static(join(process.cwd(), './assets/img/')));
  // whitelist = desable unwanted properties, transform = convert data to type needed
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      transformOptions: { enableImplicitConversion: true },
    }),
  ); // enable data control
  await app.listen(3000);
}
bootstrap();
